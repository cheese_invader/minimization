//
//  autFileReading.cpp
//  Minimization
//
//  Created by Marty on 10/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#include "autFileReading.hpp"

#include <fstream>

AutData *readFromFile(string fileName) {
    ifstream fin(fileName);

    if (!fin) {
        return nullptr;
    }
    
    AutData *data = new AutData;
    fin >> data->authType >> data->sizeX >> data-> sizeY >> data-> sizeQ;
    data->conversionTable = vector<vector<long long>>(data->sizeX, vector<long long>(data->sizeQ));
    
    if (data->authType == 2) {
        // Milly
        data->outSignalTable = vector<vector<long long>>(data->sizeX, vector<long long>(data->sizeQ));
        
        for (long long x = 0; x < data->sizeX; x++) {
            for (long long q = 0; q < data->sizeQ; q++) {
                fin >> data->conversionTable[x][q] >> data->outSignalTable[x][q];
            }
        }
    } else {
        // Mur
        data->statesY = vector<long long>(data->sizeQ);
        
        for (long long q = 0; q < data->sizeQ; q++) {
            fin >> data->statesY[q];
        }
        
        for (long long x = 0; x < data->sizeX; x++) {
            for (long long q = 0; q < data->sizeQ; q++) {
                fin >> data->conversionTable[x][q];
            }
        }
    }
    
    return data;
}
