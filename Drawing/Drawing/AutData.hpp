//
//  AutData.hpp
//  Minimization
//
//  Created by Marty on 10/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#ifndef AutData_hpp
#define AutData_hpp

#include <vector>

using namespace std;

struct AutData {
    long long authType; // Mur 1, Milly 2
    long long sizeX; // 1...
    long long sizeY; // 1...
    long long sizeQ; // 0...
    
    vector<vector<long long>> conversionTable;
    
    // Milly
    vector<vector<long long>> outSignalTable;
    
    // Mur
    vector<long long> statesY;
};
#endif /* AutData_hpp */
