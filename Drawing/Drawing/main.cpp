//
//  main.cpp
//  Drawing
//
//  Created by Marty on 13/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#include <boost/graph/graphviz.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <string>
#include <fstream>
#include <algorithm>

#include "autFileReading.hpp"

void drawGraphFromFile(string fileName, string outFileName) {
    AutData *data = readFromFile(fileName);
    
    if (data == nullptr) {
        cout << fileName << "\nReading error\n";
        return;
    }
    
    using Edge = std::pair<long long, long long>;
    using Graph = boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, boost::property<boost::vertex_color_t, boost::default_color_type>, boost::property<boost::edge_weight_t, std::string>>;
    
    std::vector<Edge> edges;
    std::vector<std::string> weights;
    
    for (long long q = 0; q < data->sizeQ; q++) {
        for (long long x = 0; x < data->sizeX; x++) {
            edges.push_back( {q, data->conversionTable[x][q]} );
            if (data->authType == 1) {
                weights.push_back(std::to_string(x + 1) + "/" + std::to_string(data->statesY[q]));
            } else {
                weights.push_back(std::to_string(x + 1) + "/" + std::to_string(data->outSignalTable[x][q]));
            }
        }
    }
    
    Graph graph(edges.begin(), edges.end(), weights.begin(), data->sizeQ);
    boost::dynamic_properties dp;
    dp.property("weight", boost::get(boost::edge_weight, graph));
    dp.property("label", boost::get(boost::edge_weight, graph));
    dp.property("node_id", boost::get(boost::vertex_index, graph));
    std::ofstream ofs(outFileName);
    boost::write_graphviz_dp(ofs, graph, dp);
}

int main(int argc, const char * argv[]) {
    drawGraphFromFile("in.txt", "dot/in.dot");
    drawGraphFromFile("out.txt", "dot/out.dot");
    
    return 0;
}


