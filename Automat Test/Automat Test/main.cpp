//
//  main.cpp
//  Automat Test
//
//  Created by Marty on 13/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#include <iostream>
#include <vector>
#include <string>

#include "AutData.hpp"
#include "autFileReading.hpp"
#include "autRun.hpp"

using namespace std;

int main(int argc, const char * argv[]) {
    long long testsNumber = 12;
    
    vector<long long>stateSizes = {
        6, 4, 3, 1, 3, 4, 12, 5, 2, 10, 3, 11
    };
    
    string ext = ".txt";
    
    string     inFilePrefix  = "in";
    string    outFilePrefix  = "out";
    string testInFilePrefix  = "test_in";
    string testOutFilePrefix = "test_out";

    for (long long test = 1; test <= testsNumber; test++) {
        string     inFileName  =     inFilePrefix  + to_string(test) + ext;
        string    outFileName  =    outFilePrefix  + to_string(test) + ext;
        string testInFileName  = testInFilePrefix  + to_string(test) + ext;
        string testOutFileName = testOutFilePrefix + to_string(test) + ext;
        
        
        // generate test_in files
//        AutData *data = readFromFile(inFileName);
//        vector<long long>testIn = generateArray(data->sizeX);
//        writeArrayIntoFile(testInFileName, testIn);
        
        
        // compute test_out_true files
//        AutData *data = readFromFile(inFileName);
//        vector<long long>testIn  = readArrayFromFile(testInFileName);
//        vector<long long>trueTestOut = run(data, testIn);
//        writeArrayIntoFile(testOutFileName, trueTestOut);

        
        AutData *data = readFromFile(outFileName);
        vector<long long> testIn      = readArrayFromFile(testInFileName);
        vector<long long> testOutTrue = readArrayFromFile(testOutFileName);
        vector<long long> testOut = run(data, testIn);


        if (testOutTrue == testOut) {
            if (stateSizes[test - 1] != data->sizeQ) {
                cout << "- not minimized" << endl;
                continue;
            }
            cout << "+" << endl;
        } else {
            cout << "-" << endl;
        }
    }
    
    return 0;
}
