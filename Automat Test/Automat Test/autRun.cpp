//
//  autRun.cpp
//  Automat Test
//
//  Created by Marty on 13/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#include "autRun.hpp"

vector<long long> run(AutData *data, vector<long long> testIn) {
    vector<long long> out(testIn.size());
    
    long long state = 0;
    
    for (long long i = 0; i < testIn.size(); i++) {
        if (data->authType == 1) {
            state = data->conversionTable[testIn[i] - 1][state];
            out[i] = data->statesY[state];
        } else {
            out[i] = data->outSignalTable[testIn[i] - 1][state];
            state = data->conversionTable[testIn[i] - 1][state];
        }
    }
    
    return out;
}

vector<long long> generateArray(long long sizeX) {
    vector<long long> arr(1000000);
    
    for (long long x = 0; x < 1000000; x++) {
        arr[x] = rand() % sizeX + 1;
    }
    
    return arr;
}

