//
//  autRun.hpp
//  Automat Test
//
//  Created by Marty on 13/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#ifndef autRun_hpp
#define autRun_hpp

#include <string>
#include <vector>
#include "AutData.hpp"

using namespace std;

vector<long long> run(AutData *data, vector<long long> testIn);

vector<long long> generateArray(long long maxX);

#endif /* autRun_hpp */
