//
//  main.cpp
//  Minimization
//
//  Created by Marty on 10/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#include <iostream>
#include "Service/coreMinimization.hpp"
#include "Service/autFileReading.hpp"
#include "Service/autFileWriting.hpp"

int main(int argc, const char * argv[]) {
    AutData *data = readFromFile("in.txt");
    
    if (data == nullptr) {
        cout << "Reading error\n";
        return -1;
    }
    
    vector<vector<long long>> clusters = divideDataByClusters(data);

    while (!split(data->conversionTable, clusters)) {}
    
    write("out.txt", data, clusters, getMarkersFromClusters(clusters));
    
    return 0;
}
