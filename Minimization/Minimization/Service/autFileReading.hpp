//
//  autFileReading.hpp
//  Minimization
//
//  Created by Marty on 10/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#ifndef autFileReading_hpp
#define autFileReading_hpp

#include <string>
#include "AutData.hpp"

using namespace std;

AutData *readFromFile(string fileName);
vector<long long>readArrayFromFile(string fileName);
void writeArrayIntoFile(string fileName, vector<long long> arr);

#endif /* autFileReading_hpp */
