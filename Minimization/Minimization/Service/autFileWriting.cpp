//
//  autFileWriting.cpp
//  Minimization
//
//  Created by Marty on 11/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#include "autFileWriting.hpp"
#include <fstream>


bool write(string fileName, AutData *data, vector<vector<long long>> clusters, vector<long long> markers) {
    ofstream fout(fileName);
    
    if (!fout) {
        return false;
    }
    
    fout << data->authType << endl;
    fout << data->sizeX << endl;
    fout << data->sizeY << endl;
    fout << clusters.size() << endl;
    
    if (data->authType == 1) {
        // Mur
        
        for (long long i = 0; i < clusters.size(); i++) {
            fout << data->statesY[clusters[i][0]] << " ";
        }
        fout << endl;
        
        for (long long x = 0; x < data->sizeX; x++) {
            for (long long q = 0; q < clusters.size(); q++) {
                long long qsample = clusters[q][0];
                long long i = data->conversionTable[x][qsample];
                
                fout << markers[i] << " ";
            }
            fout << endl;
        }
    } else {
        // Milly
        
        for (long long x = 0; x < data->sizeX; x++) {
            for (long long q = 0; q < clusters.size(); q++) {
                long long qsample = clusters[q][0];
                long long i = data->conversionTable[x][qsample];
                
                fout << markers[i] << " " << data->outSignalTable[x][qsample] << " ";
            }
            fout << endl;
        }
    }
    
    return true;
}
