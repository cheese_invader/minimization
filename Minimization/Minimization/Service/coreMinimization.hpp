//
//  coreMinimization.hpp
//  Minimization
//
//  Created by Marty on 10/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#ifndef coreMinimization_hpp
#define coreMinimization_hpp

#include <vector>
#include <set>
#include "../Entity/AutData.hpp"

using namespace std;

vector<vector<long long>> getClustersFromMarkers(vector<long long> &markers, long long numberOfClusters);
vector<long long> getMarkersFromClusters(vector<vector<long long>> &clusters);

vector<vector<long long>> divideDataByClusters(AutData *data);

// returns true when no changes in setClasses
bool split(vector<vector<long long>> &conversionTable, vector<vector<long long>> &setClasses);

#endif /* coreMinimization_hpp */
