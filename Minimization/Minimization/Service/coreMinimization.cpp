//
//  coreMinimization.cpp
//  Minimization
//
//  Created by Marty on 10/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#include "coreMinimization.hpp"



// MARK: - helpfull stuff -
vector<vector<long long>> getClustersFromMarkers(vector<long long> &markers, long long numberOfClusters) {
    vector<vector<long long>> clusters(numberOfClusters);
    
    for (long long i = 0; i < markers.size(); i++) {
        if (markers[i] < 0) {
            continue;
        }
        clusters[markers[i]].push_back(i);
    }
    
    return clusters;
}

vector<long long> getMarkersFromClusters(vector<vector<long long>> &clusters) {
    long long sizeQ = 0;
    
    for (long long row = 0; row < clusters.size(); row++) {
        sizeQ += clusters[row].size();
    }
    
    vector<long long> markers(sizeQ);
    
    for (long long row = 0; row < clusters.size(); row++) {
        for (long long i = 0; i < clusters[row].size(); i++) {
            markers[clusters[row][i]] = row;
        }
    }
    
    return markers;
}



// MARK: - raw data division -
vector<vector<long long>> divideDataByClustersMur(AutData *data) {
    vector<vector<long long>> clusters(data->sizeY);
    
    for (long long q = 0; q < data->statesY.size(); q++) {
        clusters[data->statesY[q]].push_back(q);
    }
    
    return clusters;
}

vector<vector<long long>> divideDataByClustersMilly(AutData *data) {
    vector<long long> markers(data->sizeQ, -1);
    
    long long currentMark = 0;
    
    for (long long q = 0; q < data->sizeQ; q++) {
        if (markers[q] != -1) {
            continue;
        }
        
        for (long long qf = q + 1; qf < data->sizeQ; qf++) {
            bool twins = true;
            
            for (long long x = 0; x < data->sizeX; x++) {
                if (data->outSignalTable[x][q] != data->outSignalTable[x][qf]) {
                    twins = false;
                    break;
                }
            }
            
            if (twins) {
                markers[qf] = currentMark;
            }
        }
        
        markers[q] = currentMark++;
    }
    
    return getClustersFromMarkers(markers, currentMark);
}

vector<vector<long long>> divideDataByClusters(AutData *data) {
    if (data->authType == 1) {
        return divideDataByClustersMur(data);
    } else {
        return divideDataByClustersMilly(data);
    }
}



// MARK: - split -
vector<vector<long long>> splitTableFromCluster(vector<vector<long long>> &table, vector<long long> cluster) {
    long long sizeQ = table[0].size();
    
    // other cluster
    vector<long long> markers(sizeQ, -2);
    
    for (long long i = 0; i < cluster.size(); i++) {
        markers[cluster[i]] = -1;
    }
    
    long long currentMark = 0;
    
    for (long long q = 0; q < sizeQ; q++) {
        if (markers[q] == -2 || markers[q] != -1) {
            continue;
        }
        
        for (long long qf = q + 1; qf < sizeQ; qf++) {
            if (markers[qf] == -2 || markers[qf] != -1) {
                continue;
            }
            
            bool twins = true;
            
            for (long long x = 0; x < table.size(); x++) {
                if (table[x][q] != table[x][qf]) {
                    twins = false;
                    break;
                }
            }
            if (twins) {
                markers[qf] = currentMark;
            }
        }
        markers[q] = currentMark++;
    }
    
    return getClustersFromMarkers(markers, currentMark);
}

bool split(vector<vector<long long>> &conversionTable, vector<vector<long long>> &clusters) {
    long long sizeQ = conversionTable[0].size();
    
    vector<long long> markers = getMarkersFromClusters(clusters);
    vector<vector<long long>> clusterTable(conversionTable.size(), (vector<long long>(sizeQ)));
    
    for (long long x = 0; x < conversionTable.size(); x++) {
        for (long long q = 0; q < sizeQ; q++) {
            clusterTable[x][q] = markers[conversionTable[x][q]];
        }
    }
    
    vector<vector<long long>> newCluster;
    bool changed = false;
    
    for (long long i = 0; i < clusters.size(); i++) {
        vector<vector<long long>> splittedCluster = splitTableFromCluster(clusterTable, clusters[i]);
        newCluster.insert(newCluster.end(), splittedCluster.begin(), splittedCluster.end());
        if (splittedCluster.size() != 1) {
            changed = true;
        }
    }
    
    clusters = newCluster;

    return !changed;
}
