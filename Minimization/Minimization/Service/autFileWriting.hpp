//
//  autFileWriting.hpp
//  Minimization
//
//  Created by Marty on 11/10/2018.
//  Copyright © 2018 Marty. All rights reserved.
//

#ifndef autFileWriting_hpp
#define autFileWriting_hpp

#include <string>
#include "../Entity/AutData.hpp"

using namespace std;

bool write(string fileName, AutData *data, vector<vector<long long>> clusters, vector<long long> markers);

#endif /* autFileWriting_hpp */
